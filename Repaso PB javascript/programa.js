//Comentario de una linea 
/*
Comentario de varias
lineas
*/

//variables

let a //ámbito local
var d //ámbito global

//Tipos de datos
//Primitivos
//Enteros,reales,objeto
//Estructuras
//Arreglos, cadenas de texto, objetos

a=3 //entero
console.log(a)

a='cadena'  //tipo string
console.log(a)

a='f'

a=3.2345


//Operaciones
//Calculo de un valor a partir de otro siguiendo ciertas reglas

// + suma 
// - resta
// * producto
// /cociente
// \ Modulo 

let b,c
b=56
c = a+b

console.log(c)

// Comparacion
//<,>,<=,>=,!=,==,===

a=5
b='5'


console.log("Hola como te ha ido " + a)

//operador sobre cargado 
console.log(a+b)//concatenar: unir dos cadenas de texto

//Operadores lógicos
//AND &&, OR ||

a=true
b=false

console.log(a||b)

a=8
b=7

//Decisiones
if(a<b){
    console.log("El primer numero es menor")
}
else{
    console.log("El primer numero es mayor")
}

a=5

switch(a)
{
    case 5:
        console.log("Vale 5")
        break
    case 7:
        console.log("Vale 7")
        break
    default:
        console.log("Vale un numero diferente a 5 o 7")
}
//Ciclos

for(i=0;i<10;i++)
{
    if(i!==5)
    {
        console.log(i)
    }
}
i=0
suma=0
while(i<100)
{
    suma=suma+i
    i++
}
console.log(suma)

//Arreglos

a=[1,"perro hpta",3,4,5,6]

//los elementos se diferencian por el indice de su posicion
//Posicion relativa al primer elemento del arreglo

console.log(a[1])

//Matriz: arreglo de arreglos

a=[[1],[3,5644,true,7,6],3]

console.log(a)

//funciones
function sumar(x,y)
{
    let resultado
    resultado=x+y
    return resultado
}
console.log(sumar(5,8))

//Objetos: coleccion de datos y funciones 

//Objetos predefinidos

//console
//log()
//error()
//warn()

//math

console.log(Math.PI)

console.log(Math.sin(Math.PI))





